import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-display-selector',
  templateUrl: './display-selector.component.html',
  styleUrls: ['./display-selector.component.scss']
})
export class DisplaySelectorComponent implements OnInit {

  constructor() { }

  displayType = 'grid';

  @Output() emitDisplay = new EventEmitter<string>();

  // tslint:disable-next-line: typedef
  sendDisplay() {
    this.emitDisplay.emit(this.displayType);
  }

  ngOnInit(): void {
  }
    // tslint:disable-next-line: typedef
    selectDisplay(display, not) {
      this.displayType = display;
      console.log(this.displayType);
      document.getElementById(display).classList.add('display-selector__list-grid--active');
      document.getElementById(not).classList.remove('display-selector__list-grid--active');
    }
}
