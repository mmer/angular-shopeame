import { ProductsService } from './../../../../shared/services/products.service';
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  @Input () list: any;

  constructor(private router: Router, private productsService: ProductsService) { }

  ngOnInit(): void {
  }

  // tslint:disable-next-line: typedef
  sendItemtoEdit(item) {
    this.productsService.setProduct(item);
    this.router.navigateByUrl('/management');
  }
}
