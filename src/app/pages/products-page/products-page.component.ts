import { ProductsService } from './../../shared/services/products.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-products-page',
  templateUrl: './products-page.component.html',
  styleUrls: ['./products-page.component.scss']
})
export class ProductsPageComponent implements OnInit {

  products;
  backupProd;
  search;

  display = 'grid';

  constructor(private productsService: ProductsService) { }

  ngOnInit(): void {
    this.getProducts();
  }

  // tslint:disable-next-line: typedef
  getProducts() {
    this.productsService.getProducts().subscribe(products => {
      this.products = products;
      this.backupProd = products;
    });
  }

    // tslint:disable-next-line: typedef
    toSearch(word){
      this.products = this.backupProd;
      this.products = this.products.filter(product => product.name.toLowerCase().includes(word.toLowerCase()));
    }

  // tslint:disable-next-line: typedef
  setDisplay(display) {
    this.display = display;
  }
}
