import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsPageRoutingModule } from './products-page-routing.module';
import {ProductsPageComponent} from './products-page.component';
import { GalleryComponent } from './components/gallery/gallery.component';
import { DisplaySelectorComponent } from './components/display-selector/display-selector.component';
import { ListComponent } from './components/list/list.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [ProductsPageComponent, GalleryComponent, DisplaySelectorComponent, ListComponent],
  imports: [
    CommonModule,
    ProductsPageRoutingModule,
    SharedModule,
    FormsModule
  ]
})
export class ProductsPageModule { }
