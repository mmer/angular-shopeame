import { SharedModule } from './../../shared/shared.module';
import { ManagementPageComponent } from './management-page.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManagementPageRoutingModule } from './management-page-routing.module';
import { FormComponent } from './form/form.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [ManagementPageComponent, FormComponent],
  imports: [
    CommonModule,
    ManagementPageRoutingModule,
    SharedModule,
    ReactiveFormsModule
  ]
})
export class ManagementPageModule { }
