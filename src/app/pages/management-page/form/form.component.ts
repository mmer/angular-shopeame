import { ProductsPageRoutingModule } from './../../products-page/products-page-routing.module';
import { ProductsService } from './../../../shared/services/products.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  newProduct = this.productsService.product;
  addProductForm;

  constructor(private formBuilder: FormBuilder, private productsService: ProductsService, private router: Router) { }

  ngOnInit(): void {
    this.productsService.clearProduct();
    this.addProductForm = this.formBuilder.group({
      name: [this.newProduct.name, [Validators.required]],
      price: [this.newProduct.price , [Validators.required]],
      description: [this.newProduct.description, [Validators.required]],
      stars: [this.newProduct.stars, [Validators.required]],
      image: [this.newProduct.image, [Validators.required]]
    });

    this.addProductForm.get('name').valueChanges.subscribe((name) => {
      this.newProduct.name = name;
    });
    this.addProductForm.get('price').valueChanges.subscribe((price) => {
      this.newProduct.price = price;
    });
    this.addProductForm.get('description').valueChanges.subscribe((description) => {
      this.newProduct.description = description;
    });
    this.addProductForm.get('stars').valueChanges.subscribe((stars) => {
      this.newProduct.stars = stars;
    });
    this.addProductForm.get('image').valueChanges.subscribe((image) => {
      this.newProduct.image = image;
    });
  }

  public onSubmit(): void {
    // Si el formulario es valido
    if (this.addProductForm.valid) {
          if (this.newProduct.id) {
            this.productsService.putProduct(this.newProduct, this.newProduct.id).subscribe();
            this.productsService.clearProduct();
            this.router.navigateByUrl('/products');
          } else {
            this.productsService.postProduct(this.newProduct).subscribe();
            this.router.navigateByUrl('/products');
          }
      // Reseteamos todos los campos
          this.addProductForm.reset();
          this.newProduct = {
            id: '',
            name: 'Nombre',
            price: 'Precio',
            description: 'Descripción',
            stars: 1,
            image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Antu_view-preview.svg/1200px-Antu_view-preview.svg.png',
          };
    }
  }
  // tslint:disable-next-line: typedef
  deleteItem() {
    alert('El producto se ha eliminado');
    this.productsService.deleteProduct(this.newProduct.id).subscribe();
    this.productsService.clearProduct();
    this.router.navigateByUrl('/products');
  }
}
