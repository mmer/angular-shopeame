import { ProductsService } from './../../services/products.service';
import { Component, Input, OnInit} from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {

  @Input () item;

  @Input () edit;

  constructor(private router: Router, private productsService: ProductsService) { }

  ngOnInit(): void {
  }
 // tslint:disable-next-line: typedef
  sendItemtoEdit(item) {
    this.productsService.setProduct(item);
    this.router.navigateByUrl('/management');
  }
}
