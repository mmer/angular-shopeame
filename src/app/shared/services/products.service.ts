import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class ProductsService {

  constructor(private http: HttpClient) { }

  product = {
    id: '',
    name: 'Nombre',
    price: 'Precio',
    description: 'Descripción',
    stars: 1,
    image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Antu_view-preview.svg/1200px-Antu_view-preview.svg.png',
  };

  // tslint:disable-next-line: typedef
  getProducts() {
    return this.http.get('https://shopeame-angular.herokuapp.com/products');
  }

  // Usamos esta función para enviar el producto a editar
  // tslint:disable-next-line: typedef
  setProduct(item) {
    this.product = item;
  }

  // Esta función es para resetear el producto
  // tslint:disable-next-line: typedef
  clearProduct(){
    this.product = {
      id: '',
      name: 'Nombre',
      price: 'Precio',
      description: 'Descripción',
      stars: 1,
      image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Antu_view-preview.svg/1200px-Antu_view-preview.svg.png',
    };
  }

  // Esta función añade un nuevo producto
  // tslint:disable-next-line: typedef
  postProduct(newProduct: any) {
     return this.http.post('https://shopeame-angular.herokuapp.com/products', newProduct);
  }

  // Esta función es para actualizar un producto que ya existía
  // tslint:disable-next-line: typedef
  putProduct(updatedProduct, idProd){
    return this.http.put('https://shopeame-angular.herokuapp.com/products/' + idProd, updatedProduct);
  }

  // tslint:disable-next-line: typedef
  deleteProduct(idProd){
    return this.http.delete('https://shopeame-angular.herokuapp.com/products/' + idProd);
  }
}
